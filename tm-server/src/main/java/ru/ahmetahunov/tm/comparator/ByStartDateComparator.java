package ru.ahmetahunov.tm.comparator;

import ru.ahmetahunov.tm.api.entity.IItem;
import java.util.Comparator;

public final class ByStartDateComparator<T extends IItem> implements Comparator<T> {

	@Override
	public int compare(final T o1, final T o2) {
		return o1.getStartDate().compareTo(o2.getStartDate());
	}

}

package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.entity.IItem;
import ru.ahmetahunov.tm.comparator.*;
import java.util.Comparator;

public final class ComparatorUtil {

    @NotNull
    public static <T extends IItem> Comparator<T> getComparator(@Nullable final String comparatorName) {
        if (comparatorName == null) return new ByNameComparator<>();
        switch (comparatorName) {
            case ("startDate"): return new ByStartDateComparator<>();
            case ("finishDate"): return new ByFinishDateComparator<>();
            case ("status"): return new ByStatusComparator<>();
            case ("creationDate"): return new ByCreateDateComparator<>();
            default: return new ByNameComparator<>();
        }
    }

}

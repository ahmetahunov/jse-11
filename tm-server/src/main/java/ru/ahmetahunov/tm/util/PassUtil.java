package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.property.Constant;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class PassUtil {

    @NotNull
    public static String getHash(@Nullable final String password) throws AccessForbiddenException {
        if (password == null || password.isEmpty()) throw new AccessForbiddenException("Wrong password");
        try {
            @NotNull MessageDigest encoder = MessageDigest.getInstance("MD5");
            @NotNull String result = password;
            for (int i = 0; i < Constant.CYCLE_P; i++) {
                encoder.update((Constant.SALT_P + result).getBytes());
                @NotNull byte[] buff = encoder.digest();
                result = new BigInteger(1, buff).toString(16);
                encoder.update((result + Constant.SALT_P).getBytes());
                buff = encoder.digest();
                result = new BigInteger(1, buff).toString(16);
            }
            return result;
        } catch (NoSuchAlgorithmException e) {
            throw new AccessForbiddenException(e.getMessage());
        }
    }

}

package ru.ahmetahunov.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class InterruptOperationException extends Exception {

    public InterruptOperationException() {}

    public InterruptOperationException(@Nullable final String s) {
        super(s);
    }

}

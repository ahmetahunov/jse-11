package ru.ahmetahunov.tm.property;

import org.jetbrains.annotations.NotNull;

public final class Constant {

	@NotNull
	public static final String SALT = "lkasjfAD120-31";

	@NotNull
	public static final String SALT_P = "lfmao125ak-i";

	public static final long CYCLE = 33L;

	public static final long CYCLE_P = 10L;

}

package ru.ahmetahunov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.UserEndpoint;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.UserEndpoint")
public final class UserEndpointImpl implements UserEndpoint {

	private ServiceLocator serviceLocator;

	public UserEndpointImpl() {}

	public UserEndpointImpl(@NotNull ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public User createUser(
			@WebParam(name = "login") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException {
		@NotNull final IUserService userService = serviceLocator.getUserService();
		if (userService.contains(login)) throw new AccessForbiddenException("User with this login exists.");
		@NotNull final User user = new User();
		user.setLogin(login);
		user.setPassword(PassUtil.getHash(password));
		return serviceLocator.getUserService().persist(user);
	}

	@Override
	@WebMethod
	public void updatePassword(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "old") final String oldPassword,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getUserService().updatePassword(session.getUserId(), oldPassword, password);
	}

	@Override
	@WebMethod
	public void updateLogin(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "login") final String login
	) throws AccessForbiddenException, InterruptOperationException {
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getUserService().updateLogin(session.getUserId(), login);
	}

	@Nullable
	@Override
	@WebMethod
	public User findUser(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getUserService().findOne(session.getUserId());
	}

}

package ru.ahmetahunov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.TaskEndpoint;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.TaskEndpoint")
public final class TaskEndpointImpl implements TaskEndpoint {

	private ServiceLocator serviceLocator;

	public TaskEndpointImpl() {}

	public TaskEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public Task createTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "task") final Task task
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		task.setUserId(session.getUserId());
		return serviceLocator.getTaskService().persist(task);
	}

	@Nullable
	@Override
	@WebMethod
	public Task updateTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "task") final Task task
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		task.setUserId(session.getUserId());
		return serviceLocator.getTaskService().merge(task);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findAllTasks(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		@NotNull final Comparator<Task> comparator = ComparatorUtil.getComparator(comparatorName);
		return serviceLocator.getTaskService().findAll(session.getUserId(), comparator);
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findAllProjectTasks(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findAll(projectId, session.getUserId());
	}

	@Nullable
	@Override
	@WebMethod
	public Task findOneTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findOne(taskId, session.getUserId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByName(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "taskName") final String taskName
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findByName(taskName, session.getUserId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByDescription(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findByDescription(description, session.getUserId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Task> findTaskByNameOrDesc(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().findByNameOrDesc(searchPhrase, session.getUserId());
	}

	@Override
	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "session") final Session session
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getTaskService().removeAll(session.getUserId());
	}

	@Nullable
	@Override
	@WebMethod
	public Task removeTask(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "taskId") final String taskId
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getTaskService().remove(taskId, session.getUserId());
	}

}

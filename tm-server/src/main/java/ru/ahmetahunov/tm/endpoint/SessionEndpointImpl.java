package ru.ahmetahunov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.SessionEndpoint;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.util.PassUtil;
import ru.ahmetahunov.tm.util.SessionSignatureUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.SessionEndpoint")
public final class SessionEndpointImpl implements SessionEndpoint {

	private ServiceLocator serviceLocator;

	public SessionEndpointImpl() {}

	public SessionEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@NotNull
	@Override
	@WebMethod
	public Session createSession(
			@WebParam(name = "login") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException {
		@Nullable final User user = serviceLocator.getUserService().findUser(login);
		if (user == null || password == null) throw new AccessForbiddenException("Access denied. User does not exist.");
		@NotNull final String hash = PassUtil.getHash(password);
		if (!user.getPassword().equals(hash)) throw new AccessForbiddenException("Wrong password");
		@NotNull final Session session = new Session();
		session.setUserId(user.getId());
		session.setRole(user.getRole());
		session.setSignature(SessionSignatureUtil.sign(session));
		serviceLocator.getSessionService().persist(session);
		return session;
	}

	@Override
	@WebMethod
	public void removeSession(@WebParam(name = "session") final Session session) {
		serviceLocator.getSessionService().remove(session.getId());
	}

}

package ru.ahmetahunov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint")
public final class ProjectEndpointImpl implements ProjectEndpoint {

	private ServiceLocator serviceLocator;

	public ProjectEndpointImpl() {}

	public ProjectEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Nullable
	@Override
	@WebMethod
	public Project createProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "project") final Project project
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		project.setUserId(session.getUserId());
		return serviceLocator.getProjectService().persist(project);
	}

	@Nullable
	@Override
	@WebMethod
	public Project updateProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "project") final Project project
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		project.setUserId(session.getUserId());
		return serviceLocator.getProjectService().merge(project);
	}

	@Nullable
	@Override
	@WebMethod
	public Project findOneProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findOne(projectId, session.getUserId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByName(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectName") final String projectName
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findByName(projectName, session.getUserId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByDescription(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "description") final String description
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findByDescription(description, session.getUserId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findProjectByNameOrDesc(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "searchPhrase") final String searchPhrase
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		return serviceLocator.getProjectService().findByNameOrDesc(searchPhrase, session.getUserId());
	}

	@NotNull
	@Override
	@WebMethod
	public List<Project> findAllProjects(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "comparatorName") final String comparatorName
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		@NotNull final Comparator<Project> comparator = ComparatorUtil.getComparator(comparatorName);
		return serviceLocator.getProjectService().findAll(session.getUserId(), comparator);
	}

	@Override
	@WebMethod
	public void removeAllProjects(@WebParam(name = "session") final Session session) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		@NotNull final ITaskService taskService = serviceLocator.getTaskService();
		@NotNull final List<Project> projects = serviceLocator.getProjectService().findAll(session.getUserId());
		for (@NotNull Project project : projects)
			taskService.removeAllProjectTasks(project.getId(), session.getUserId());
		serviceLocator.getProjectService().removeAll(session.getUserId());
	}

	@Nullable
	@Override
	@WebMethod
	public Project removeProject(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException {
		serviceLocator.getSessionService().validate(session);
		serviceLocator.getTaskService().removeAllProjectTasks(projectId, session.getUserId());
		return serviceLocator.getProjectService().remove(projectId, session.getUserId());
	}

}

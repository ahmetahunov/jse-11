package ru.ahmetahunov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.entity.User;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findUser(@NotNull final String login) {
        for (@NotNull User user : findAll()) {
            if (user.getLogin().equalsIgnoreCase(login)) return user;
        }
        return null;
    }

    @Override
    public void removeAll() {
        collection.clear();
    }

    @Override
    public boolean contains(@NotNull final String login) {
        return findUser(login) != null;
    }

}

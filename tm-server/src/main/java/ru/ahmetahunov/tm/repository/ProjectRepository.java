package ru.ahmetahunov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.entity.Project;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        for (@NotNull Project project : findAll(userId)) {
            remove(project.getId());
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> projects = new LinkedList<>();
        for (@NotNull Project project : findAll()) {
            if (project.getUserId().equals(userId)) projects.add(project);
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator) {
        @NotNull final List<Project> projects = new LinkedList<>();
        for (@NotNull Project project : findAll()) {
            if (project.getUserId().equals(userId)) projects.add(project);
        }
        projects.sort(comparator);
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String projectId, @NotNull final String userId) {
        @Nullable Project project = collection.get(projectId);
        if (project == null || !project.getUserId().equals(userId)) return null;
        return project;
    }

    @NotNull
    @Override
    public List<Project> findByName(@NotNull final String projectName, @NotNull final String userId) {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull Project project : findAll()) {
            if (project.getUserId().equals(userId) && project.getName().contains(projectName))
                projects.add(project);
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByDescription(@NotNull final String description, @NotNull final String userId) {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull Project project : findAll()) {
            if (project.getUserId().equals(userId) && project.getDescription().contains(description))
                projects.add(project);
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(@NotNull final String searchPhrase, @NotNull final String userId) {
        @NotNull final List<Project> projects = new ArrayList<>();
        for (@NotNull Project project : findAll()) {
            if (!project.getUserId().equals(userId)) continue;
            if (project.getDescription().contains(searchPhrase) || project.getName().contains(searchPhrase))
                projects.add(project);
        }
        return projects;
    }

    @Override
    public boolean contains(@NotNull final String projectId, @NotNull final String userId) {
        @Nullable final Project project = collection.get(projectId);
        if (project == null) return false;
        return project.getUserId().equals(userId);
    }

}

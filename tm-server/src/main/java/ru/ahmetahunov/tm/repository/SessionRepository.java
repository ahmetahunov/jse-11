package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.entity.Session;
import java.util.Objects;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

	@Nullable
	@Override
	public Session findOne(@NotNull final String id, @NotNull final String userId) {
		@Nullable final Session session = collection.get(id);
		if (session == null || !userId.equals(session.getUserId())) return null;
		return session;
	}

	@Nullable
	@Override
	public Session remove(@NotNull final String id, @NotNull final String userId) {
		@Nullable final Session session = collection.get(id);
		if (session == null || !userId.equals(session.getUserId())) return null;
		return collection.remove(id);
	}

	@Override
	public boolean contains(@NotNull final Session session) {
		@Nullable final Session checkSession = collection.get(session.getId());
		if (checkSession == null) return false;
		return Objects.equals(checkSession.getUserId(), session.getUserId())
				&& Objects.equals(checkSession.getSignature(), session.getSignature())
				&& Objects.equals(checkSession.getCreateDate(), session.getCreateDate())
				&& Objects.equals(checkSession.getRole(), session.getRole());
	}

}

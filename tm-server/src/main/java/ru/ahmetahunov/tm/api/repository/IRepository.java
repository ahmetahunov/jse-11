package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import java.util.Collection;
import java.util.List;

public interface IRepository<T extends AbstractEntity> {

    @Nullable
    public T persist(T item) throws IdCollisionException;

    @Nullable
    public T merge(T item);

    @Nullable
    public T remove(String id);

    @NotNull
    public List<T> findAll();

    @Nullable
    public T findOne(String id);

    public void load(Collection<T> data);

}

package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;

public interface ISessionService extends IAbstractService<Session> {

	@Nullable
	public Session findOne(String id, String userId);

	@Nullable
	public Session remove(String id, String userId);

	public void validate(Session session) throws AccessForbiddenException;

	public void validate(Session session, Role role) throws AccessForbiddenException;

}

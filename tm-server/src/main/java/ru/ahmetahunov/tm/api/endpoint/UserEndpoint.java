package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface UserEndpoint {

	@Nullable
	@WebMethod
	public User createUser(
			@WebParam(name = "login") String login,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException;

	@WebMethod
	public void updatePassword(
			@WebParam(name = "session") Session session,
			@WebParam(name = "old") String oldPassword,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException;

	@WebMethod
	public void updateLogin(
			@WebParam(name = "session") Session session,
			@WebParam(name = "login") String login
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public User findUser(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException;

}

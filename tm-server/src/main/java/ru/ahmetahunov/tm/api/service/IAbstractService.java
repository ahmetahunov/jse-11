package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.AbstractEntity;
import java.util.Collection;
import java.util.List;

public interface IAbstractService<T extends AbstractEntity> {

    @Nullable
    public T persist(T item);

    @Nullable
    public T merge(T item);

    @Nullable
    public T remove(String id);

    @Nullable
    public T remove(T item);

    @Nullable
    public T findOne(String id);

    @NotNull
    public List<T> findAll();

    public void load(Collection<T> data);

}

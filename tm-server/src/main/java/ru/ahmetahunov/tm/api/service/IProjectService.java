package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IAbstractService<Project> {

    @Nullable
    public Project findOne(String projectId, String userId);

    @NotNull
    public List<Project> findByName(String projectName, String userId);

    @NotNull
    public List<Project> findByDescription(String description, String userId);

    @NotNull
    public List<Project> findByNameOrDesc(String searchPhrase, String userId);

    @NotNull
    public List<Project> findAll(String userId);

    @NotNull
    public List<Project> findAll(String userId, Comparator<Project> comparator);

    public void removeAll(String userId);

    @Nullable
    public Project remove(String projectId, String userId);

}

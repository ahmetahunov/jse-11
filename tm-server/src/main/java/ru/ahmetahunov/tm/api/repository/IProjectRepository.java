package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    public void removeAll(String userId);

    @NotNull
    public List<Project> findAll(String userId);

    @NotNull
    public List<Project> findAll(String userId, Comparator<Project> comparator);

    @Nullable
    public Project findOne(String projectName, String userId);

    @NotNull
    public List<Project> findByName(String projectName, String userId);

    @NotNull
    public List<Project> findByDescription(String description, String userId);

    @NotNull
    public List<Project> findByNameOrDesc(String searchPhrase, String userId);

    public boolean contains(String projectId, String userId);

}

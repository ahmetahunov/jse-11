package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface TaskEndpoint {

	@Nullable
	@WebMethod
	public Task createTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "task") Task task
	) throws AccessForbiddenException;

	@Nullable
	@WebMethod
	public Task updateTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "task") Task task
	) throws AccessForbiddenException;

	@NotNull
	@WebMethod
	public List<Task> findAllTasks(
			@WebParam(name = "session") Session session,
			@WebParam(name = "comparator") String comparator
	) throws AccessForbiddenException;

	@NotNull
	@WebMethod
	public List<Task> findAllProjectTasks(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "projectId") final String projectId
	) throws AccessForbiddenException;

	@Nullable
	@WebMethod
	public Task findOneTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByName(
			@WebParam(name = "session") Session session,
			@WebParam(name = "taskName") String taskName
	) throws AccessForbiddenException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByDescription(
			@WebParam(name = "session") Session session,
			@WebParam(name = "description") String description
	) throws AccessForbiddenException;

	@NotNull
	@WebMethod
	public List<Task> findTaskByNameOrDesc(
			@WebParam(name = "session") Session session,
			@WebParam(name = "searchPhrase") String searchPhrase
	) throws AccessForbiddenException;

	@WebMethod
	public void removeAllTasks(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException;

	@Nullable
	@WebMethod
	public Task removeTask(
			@WebParam(name = "session") Session session,
			@WebParam(name = "taskId") String taskId
	) throws AccessForbiddenException;

}

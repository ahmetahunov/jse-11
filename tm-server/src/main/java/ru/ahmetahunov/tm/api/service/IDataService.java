package ru.ahmetahunov.tm.api.service;

import ru.ahmetahunov.tm.exception.InterruptOperationException;
import java.io.IOException;

public interface IDataService {

	public void dataSaveBin() throws IOException;

	public void dataSaveXmlJaxb() throws IOException, InterruptOperationException;

	public void dataSaveXmlJackson() throws IOException;

	public void dataSaveJsonJaxb() throws IOException, InterruptOperationException;

	public void dataSaveJsonJackson() throws IOException;

	public void dataLoadBin() throws InterruptOperationException, IOException, ClassNotFoundException;

	public void dataLoadXmlJaxb() throws InterruptOperationException, IOException;

	public void dataLoadXmlJackson() throws InterruptOperationException, IOException;

	public void dataLoadJsonJaxb() throws InterruptOperationException;

	public void dataLoadJsonJackson() throws InterruptOperationException, IOException;

}

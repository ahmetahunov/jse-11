package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface SessionEndpoint {

	@Nullable
	@WebMethod
	public Session createSession(
			@WebParam(name = "login") String login,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException;

	@WebMethod
	public void removeSession(@WebParam(name = "session") Session session);

}

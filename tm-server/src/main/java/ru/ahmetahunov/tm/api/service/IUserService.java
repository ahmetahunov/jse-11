package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;

public interface IUserService extends IAbstractService<User> {

    @Nullable
    public User findUser(String login);

    public boolean contains(String login);

    public void updatePasswordAdmin(String userId, String password) throws InterruptOperationException, AccessForbiddenException;

    public void updateRole(String userId, Role role) throws InterruptOperationException;

    public void updatePassword(String userId, String oldPassword, String newPassword) throws AccessForbiddenException;

    public void updateLogin(String userId, String login) throws AccessForbiddenException, InterruptOperationException;

}

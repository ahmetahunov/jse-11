package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

	@Nullable
	public Session findOne(String id, String userId);

	@Nullable
	public Session remove(String id, String userId);

	public boolean contains(Session session);

}

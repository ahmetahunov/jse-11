package ru.ahmetahunov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@WebService
public interface AdminEndpoint {

	@WebMethod
	public void dataSaveBin(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, IOException;

	@WebMethod
	public void dataSaveXmlJaxb(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException;

	@WebMethod
	public void dataSaveXmlJackson(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, IOException;

	@WebMethod
	public void dataSaveJsonJaxb(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException;

	@WebMethod
	public void dataSaveJsonJackson(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, IOException;

	@WebMethod
	public void dataLoadBin(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, InterruptOperationException, IOException, ClassNotFoundException;

	@WebMethod
	public void dataLoadXmlJaxb(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, InterruptOperationException, IOException;

	@WebMethod
	public void dataLoadXmlJackson(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, IOException, InterruptOperationException;

	@WebMethod
	public void dataLoadJsonJaxb(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, InterruptOperationException;

	@WebMethod
	public void dataLoadJsonJackson(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException, InterruptOperationException, IOException;

	@NotNull
	@WebMethod
	public List<User> findAllUsers(
			@WebParam(name = "session") Session session
	) throws AccessForbiddenException;

	@WebMethod
	void userUpdatePasswordAdmin(
			@WebParam(name = "session") Session session,
			@WebParam(name = "userId") String userId,
			@WebParam(name = "password") String password
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public User userRegisterAdmin(
			@WebParam(name = "session") final Session session,
			@WebParam(name = "user") final String login,
			@WebParam(name = "password") final String password
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public User userFindByLogin(
			@WebParam(name = "session") Session session,
			@WebParam(name = "login") String login
	) throws AccessForbiddenException;

	@WebMethod
	public void userChangeRoleAdmin(
			@WebParam(name = "session") Session session,
			@WebParam(name = "userId") String userId,
			@WebParam(name = "role") Role role
	) throws AccessForbiddenException, InterruptOperationException;

	@Nullable
	@WebMethod
	public User userRemove(
			@WebParam(name = "session") Session session,
			@WebParam(name = "userId") String userId
	) throws AccessForbiddenException;

}

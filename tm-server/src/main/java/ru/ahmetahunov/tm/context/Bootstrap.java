package ru.ahmetahunov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.*;
import ru.ahmetahunov.tm.endpoint.*;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.SessionRepository;
import ru.ahmetahunov.tm.repository.TaskRepository;
import ru.ahmetahunov.tm.repository.UserRepository;
import ru.ahmetahunov.tm.service.*;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.xml.ws.Endpoint;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IDataService dataService = new DataService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointImpl(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointImpl(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointImpl(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointImpl(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpointImpl(this);

    public void init() throws Exception {
        createDefaultUsers();
        startServer();
    }

    private void startServer() {
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl", userEndpoint);
        System.out.println("http://localhost:8080/UserEndpoint?wsdl     is running...");
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl", sessionEndpoint);
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl  is running...");
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl", taskEndpoint);
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl     is running...");
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl", projectEndpoint);
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl  is running...");
        Endpoint.publish("http://localhost:8080/AdminEndpoint?wsdl", adminEndpoint);
        System.out.println("http://localhost:8080/AdminEndpoint?wsdl    is running...");
    }

    private void createDefaultUsers() throws NoSuchAlgorithmException, AccessForbiddenException {
        @NotNull final User user = new User();
        user.setId("64a8bae7-e916-4d1d-9b1e-04bc19d0bd91");
        user.setLogin("user");
        user.setPassword(PassUtil.getHash("0000"));
        userService.persist(user);
        @NotNull final User admin = new User();
        admin.setId("3d8043c1-6aeb-4df9-af8d-7a8de0f99a09");
        admin.setLogin("admin");
        admin.setPassword(PassUtil.getHash("admin"));
        admin.setRole(Role.ADMINISTRATOR);
        userService.persist(admin);
    }

}

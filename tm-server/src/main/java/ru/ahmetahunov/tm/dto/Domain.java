package ru.ahmetahunov.tm.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonRootName("Domain")
@XmlRootElement(name = "Domain")
public final class Domain implements Serializable {

	private static final long serialVersionUID = 1L;

	@Nullable
	@XmlElement(name = "project")
	@XmlElementWrapper(name = "projects")
	@JacksonXmlProperty(localName = "project")
	@JacksonXmlElementWrapper(localName = "projects")
	private List<Project> projects;

	@Nullable
	@XmlElement(name = "task")
	@XmlElementWrapper(name = "tasks")
	@JacksonXmlProperty(localName = "task")
	@JacksonXmlElementWrapper(localName = "tasks")
	private List<Task> tasks;

	@Nullable
	@XmlElement(name = "user")
	@XmlElementWrapper(name = "users")
	@JacksonXmlProperty(localName = "user")
	@JacksonXmlElementWrapper(localName = "users")
	private List<User> users;

	public void load(@NotNull final ServiceLocator serviceLocator) {
		@NotNull final IProjectService projectService = serviceLocator.getProjectService();
		@NotNull final ITaskService taskService = serviceLocator.getTaskService();
		@NotNull final IUserService userService = serviceLocator.getUserService();
		this.projects = projectService.findAll();
		this.tasks = taskService.findAll();
		this.users = userService.findAll();
	}

	public void upload(@NotNull final ServiceLocator serviceLocator) {
		@NotNull final IProjectService projectService = serviceLocator.getProjectService();
		@NotNull final ITaskService taskService = serviceLocator.getTaskService();
		@NotNull final IUserService userService = serviceLocator.getUserService();
		projectService.load(projects);
		taskService.load(tasks);
		userService.load(users);
	}

}

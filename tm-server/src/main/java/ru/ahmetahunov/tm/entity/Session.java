package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.enumerated.Role;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class Session extends AbstractEntity {

	@Nullable
	private String userId;

	@Nullable
	private Role role;

	@NotNull
	private Date createDate = new Date(System.currentTimeMillis());

	@Nullable
	private String signature;

}

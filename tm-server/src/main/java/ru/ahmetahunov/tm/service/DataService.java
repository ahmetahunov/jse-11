package ru.ahmetahunov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.*;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.service.IDataService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.dto.Domain;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

public final class DataService implements IDataService {

	@NotNull
	final ServiceLocator serviceLocator;

	public DataService(@NotNull final ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	@Override
	public void dataSaveBin() throws IOException {
		@NotNull final File file = prepareDirAndFile("/repositories.bin");
		@NotNull final FileOutputStream fos = new FileOutputStream(file);
		@NotNull final ObjectOutputStream stream = new ObjectOutputStream(fos);
		@NotNull final Domain data = new Domain();
		data.load(serviceLocator);
		stream.writeObject(data);
		stream.flush();
		stream.close();
		fos.close();
	}

	@Override
	public void dataSaveXmlJaxb() throws IOException, InterruptOperationException {
		try {
			@NotNull final File file = prepareDirAndFile("/repo_jaxb.xml");
			@NotNull final FileOutputStream fos = new FileOutputStream(file);
			@NotNull final JAXBContext context =
					(JAXBContext) JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
			@NotNull final JAXBMarshaller marshaller = context.createMarshaller();
			@NotNull final Domain data = new Domain();
			data.load(serviceLocator);
			marshaller.setProperty(JAXBMarshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_XML);
			marshaller.marshal(data, fos);
			fos.close();
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new InterruptOperationException("JAXB failed.");
		}
	}

	@Override
	public void dataSaveXmlJackson() throws IOException {
		@NotNull final File file = prepareDirAndFile("/repo_jackson.xml");
		@NotNull final ObjectMapper mapper = new XmlMapper();
		@NotNull final Domain data = new Domain();
		data.load(serviceLocator);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
		mapper.writerWithDefaultPrettyPrinter().writeValue(file, data);
	}

	@Override
	public void dataSaveJsonJaxb() throws IOException, InterruptOperationException {
		try {
			@NotNull final File file = prepareDirAndFile("/repo_jaxb.json");
			@NotNull final FileOutputStream fos = new FileOutputStream(file);
			@NotNull final JAXBContext context =
					(JAXBContext) JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
			@NotNull final JAXBMarshaller marshaller = context.createMarshaller();
			@NotNull final Domain data = new Domain();
			data.load(serviceLocator);
			marshaller.setProperty(JAXBMarshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
			marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, false);
			marshaller.setProperty(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);
			marshaller.marshal(data, fos);
			fos.close();
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new InterruptOperationException("JAXB failed.");
		}
	}

	@Override
	public void dataSaveJsonJackson() throws IOException {
		@NotNull final File file = prepareDirAndFile("/repo_jackson.json");
		@NotNull final ObjectMapper mapper = new ObjectMapper();
		@NotNull final Domain data = new Domain();
		data.load(serviceLocator);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));
		mapper.writerWithDefaultPrettyPrinter().writeValue(file, data);
	}

	@Override
	public void dataLoadBin() throws InterruptOperationException, IOException, ClassNotFoundException {
		@NotNull final File file = checkDirAndFile("/repositories.bin");
		@NotNull final FileInputStream fis = new FileInputStream(file);
		@NotNull final ObjectInputStream reader = new ObjectInputStream(fis);
		@NotNull final Domain data = (Domain) reader.readObject();
		data.upload(serviceLocator);
		reader.close();
		fis.close();
	}

	@Override
	public void dataLoadXmlJaxb() throws InterruptOperationException, IOException {
		try {
			@NotNull final File file = checkDirAndFile("/repo_jaxb.xml");
			@NotNull final FileInputStream fis = new FileInputStream(file);
			@NotNull final JAXBContext context =
					(JAXBContext) JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
			@NotNull final JAXBUnmarshaller unmarshaller = context.createUnmarshaller();
			unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_XML);
			@NotNull final Domain data = (Domain) unmarshaller.unmarshal(fis);
			data.upload(serviceLocator);
			fis.close();
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new InterruptOperationException("JAXB failed.");
		}
	}

	@Override
	public void dataLoadXmlJackson() throws InterruptOperationException, IOException {
		@NotNull final File file = checkDirAndFile("/repo_jackson.xml");
		@NotNull final ObjectMapper mapper = new XmlMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		@NotNull final Domain data = mapper.readValue(file, Domain.class);
		data.upload(serviceLocator);
	}

	@Override
	public void dataLoadJsonJaxb() throws InterruptOperationException {
		try {
			@NotNull final File file = checkDirAndFile("/repo_jaxb.json");
			@NotNull final StreamSource fis = new StreamSource(file);
			@NotNull final JAXBContext context =
					(JAXBContext) JAXBContextFactory.createContext(new Class[]{Domain.class}, null);
			@NotNull final JAXBUnmarshaller unmarshaller = context.createUnmarshaller();
			unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
			unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, false);
			unmarshaller.setProperty(UnmarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);
			@NotNull final Domain data = (Domain) unmarshaller.unmarshal(fis, Domain.class).getValue();
			data.upload(serviceLocator);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new InterruptOperationException("JAXB failed.");
		}
	}

	@Override
	public void dataLoadJsonJackson() throws InterruptOperationException, IOException {
		@NotNull final File file = checkDirAndFile("/repo_jackson.json");
		@NotNull final ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		@NotNull final Domain data = mapper.readValue(file, Domain.class);
		data.upload(serviceLocator);
	}

	@NotNull
	private File prepareDirAndFile(@NotNull final String fileName) throws IOException {
		@NotNull final Path dir = Paths.get("../data");
		if (!Files.exists(dir)) Files.createDirectory(dir);
		@NotNull final Path file = Paths.get(dir + fileName);
		if (!Files.exists(file)) Files.createFile(file);
		return file.toFile();
	}

	@NotNull
	private File checkDirAndFile(@NotNull final String fileName) throws InterruptOperationException {
		@NotNull final Path dir = Paths.get("../data");
		@NotNull final Path file = Paths.get(dir + fileName);
		if (!Files.exists(dir) || !Files.exists(file)) throw new InterruptOperationException("File does not exist.");
		return file.toFile();
	}

}

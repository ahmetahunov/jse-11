package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptOperationException;
import ru.ahmetahunov.tm.util.PassUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository repository;

    public UserService(@NotNull final IUserRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Nullable
    @Override
    public User findUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return repository.findUser(login);
    }

    @Override
    public boolean contains(@Nullable final String login) {
        if (login == null || login.isEmpty()) return true;
        return repository.contains(login);
    }

    @Override
    public void updatePasswordAdmin(@Nullable final String userId, @Nullable final String password)
            throws InterruptOperationException, AccessForbiddenException {
        if (userId == null || userId.isEmpty()) throw new InterruptOperationException("This user does not exist.");
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new InterruptOperationException("This user does not exist.");
        @NotNull final String hash = PassUtil.getHash(password);
        user.setPassword(hash);
        repository.merge(user);
    }

    @Override
    public void updateRole(@Nullable final String userId, @Nullable final Role role)
            throws InterruptOperationException {
        if (userId == null || userId.isEmpty()) throw new InterruptOperationException("This user does not exist.");
        if (role == null) throw new InterruptOperationException("Unknown role.");
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new InterruptOperationException("This user does not exist.");
        user.setRole(role);
        repository.merge(user);
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String oldPassword,
            @Nullable final String password
    ) throws AccessForbiddenException {
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new AccessForbiddenException("This user does not exist.");
        @NotNull final String hash = PassUtil.getHash(oldPassword);
        if (!user.getPassword().equals(hash)) throw new AccessForbiddenException("Wrong password.");
        @NotNull final String newHash = PassUtil.getHash(password);
        user.setPassword(newHash);
        repository.merge(user);
    }

    @Override
    public void updateLogin(
            @Nullable final String userId,
            @Nullable final String login
    ) throws AccessForbiddenException, InterruptOperationException {
        if (login == null || login.isEmpty()) throw new InterruptOperationException("Wrong format login.");
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new AccessForbiddenException("This user does not exist.");
        if (repository.contains(login)) throw new InterruptOperationException("This login already exists.");
        user.setLogin(login);
        repository.merge(user);
    }

}

package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ISessionRepository;
import ru.ahmetahunov.tm.api.service.ISessionService;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;

public final class SessionService extends AbstractService<Session> implements ISessionService {

	@NotNull
	private final ISessionRepository repository;

	public SessionService(@NotNull ISessionRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Nullable
	@Override
	public Session findOne(@Nullable final String id, @Nullable final String userId) {
		if (id == null || id.isEmpty()) return null;
		if (userId == null || userId.isEmpty()) return null;
		return repository.findOne(id, userId);
	}

	@Nullable
	@Override
	public Session remove(@Nullable final String id, @Nullable final String userId) {
		if (id == null || id.isEmpty()) return null;
		if (userId == null || userId.isEmpty()) return null;
		return repository.remove(id, userId);
	}

	@Override
	public void validate(@Nullable final Session session) throws AccessForbiddenException {
		if (session == null) throw new AccessForbiddenException("Access denied!");
		final boolean isUserIdNull = session.getUserId() == null;
		final boolean isSignatureNull = session.getSignature() == null;
		final boolean isRoleNull = session.getRole() == null;
		if (isUserIdNull || isSignatureNull || isRoleNull) throw new AccessForbiddenException("Access denied!");
		if (!repository.contains(session)) throw new AccessForbiddenException("Access denied!");
		final long existTime = System.currentTimeMillis() - session.getCreateDate().getTime();
		if (existTime > 32400000) {
			repository.remove(session.getId());
			throw new AccessForbiddenException("Access denied! Session has expired.");
		}
	}

	@Override
	public void validate(@Nullable final Session session, @NotNull final Role role) throws AccessForbiddenException {
		validate(session);
		if (!role.equals(session.getRole())) throw new AccessForbiddenException("Access denied!");
	}

}

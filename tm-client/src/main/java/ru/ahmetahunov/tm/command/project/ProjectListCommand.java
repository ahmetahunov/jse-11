package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all available projects";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        int i = 1;
        terminalService.writeMessage("[PROJECT LIST]");
        @NotNull final String comparator = terminalService.getAnswer(
                "Enter sort type<creationDate|startDate|finishDate|status>: "
        );
        for (@NotNull final Project project : projectEndpoint.findAllProjects(session, comparator)) {
            terminalService.writeMessage(String.format("%d. %s ID:%s", i++, project.getName(), project.getId()));
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectEditDatesCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-edit-dates";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit start and finish dates in selected project.";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception, FailedOperationException {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT DATES]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project project = projectEndpoint.findOneProject(session, projectId);
        if (project == null) throw new FailedOperationException("Selected project does not exist.");
        @NotNull final String startDate = terminalService.getAnswer("Please enter new start date: ");
        @NotNull final String finishDate = terminalService.getAnswer("Please enter new finish date: ");
        project.setStartDate(DateUtil.parseDate(startDate));
        project.setFinishDate(DateUtil.parseDate(finishDate));
        projectEndpoint.updateProject(session, project);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}

package ru.ahmetahunov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return true; }

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Exit from Task Manager.";
    }

    public void execute() {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session != null) {
            serviceLocator.getSessionEndpoint().removeSession(session);
            serviceLocator.getStateService().setSession(null);
        }
        serviceLocator.getTerminalService().writeMessage("Have a nice day!");
    }

}
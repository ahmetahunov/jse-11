package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.StatusUtil;
import java.io.IOException;

@NoArgsConstructor
public final class ProjectEditStatusCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-edit-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit status of project.";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception, FailedOperationException {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT PROJECT STATUS]");
        @NotNull final String projectId = terminalService.getAnswer("Please enter project id: ");
        @Nullable final Project project = projectEndpoint.findOneProject(session, projectId);
        if (project == null) throw new FailedOperationException("Selected project does not exist");
        @NotNull final String statusName = terminalService.getAnswer("Enter status<planned|in-progress|done>: ");
        @Nullable final Status status = StatusUtil.getStatus(statusName);
        project.setStatus(status);
        projectEndpoint.updateProject(session, project);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}

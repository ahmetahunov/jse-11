package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.AccessForbiddenException_Exception;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all user's available tasks.";
    }

    @Override
    public void execute() throws AccessForbiddenException_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        serviceLocator.getTaskEndpoint().removeAllTasks(session);
        serviceLocator.getTerminalService().writeMessage("[ALL TASKS REMOVED]");
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws IOException, FailedOperationException, AccessForbiddenException_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASK CREATE]");
        @NotNull final String projectId = terminalService.getAnswer("Enter project id: ");
        @NotNull final String taskName = terminalService.getAnswer("Please enter task name: ");
        if (taskName.isEmpty()) throw new FailedOperationException("Name cannot be empty.");
        @NotNull final String description = terminalService.getAnswer("Please enter description: ");
        @NotNull final String startDate =
                terminalService.getAnswer("Please enter start date(example: 01.01.2020): ");
        @NotNull final String finishDate =
                terminalService.getAnswer("Please enter finish date(example: 01.01.2020): ");
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(description);
        task.setStartDate(DateUtil.parseDate(startDate));
        task.setFinishDate(DateUtil.parseDate(finishDate));
        task.setProjectId(projectId);
        serviceLocator.getTaskEndpoint().createTask(session, task);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}
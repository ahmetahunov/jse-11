package ru.ahmetahunov.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public class DataLoadJaxbJsonCommand extends AbstractCommand {

	@Override
	public boolean isSecure() {
		return false;
	}

	@NotNull
	@Override
	public String getName() {
		return "data-load-jaxb-json";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Load data from json file (JAXB).";
	}

	@Override
	public void execute() throws Exception {
		@Nullable final Session session = serviceLocator.getStateService().getSession();
		serviceLocator.getAdminEndpoint().dataLoadJsonJaxb(session);
		serviceLocator.getTerminalService().writeMessage("[LOAD COMPLETE]");
	}

	@Nullable
	@Override
	public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}

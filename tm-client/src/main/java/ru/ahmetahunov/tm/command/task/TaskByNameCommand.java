package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class TaskByNameCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-by-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks with entered part of name.";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASKS BY NAME]");
        @NotNull final String taskName = terminalService.getAnswer("Please enter task name: ");
        @NotNull final List<Task> tasks = taskEndpoint.findTaskByName(session, taskName);
        int i = 1;
        for (@NotNull final Task task : tasks) {
            terminalService.writeMessage(String.format("%d.%s ID:%s", i++, task.getName(), task.getId()));
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}

package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.AccessForbiddenException_Exception;
import ru.ahmetahunov.tm.api.endpoint.InterruptOperationException_Exception;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.util.RoleUtil;

@NoArgsConstructor
public final class UserChangeAccessAdminCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "change-user-access";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change selected user's access.";
    }

    @Override
    public void execute() throws Exception, AccessForbiddenException_Exception, InterruptOperationException_Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        terminalService.writeMessage("[CHANGE USER'S ACCESS RIGHTS]");
        @NotNull final String userId = terminalService.getAnswer("Please enter user's id: ");
        @NotNull final String answer = terminalService.getAnswer("Please enter role<User/Administrator>: ");
        @Nullable final Role role = RoleUtil.getRole(answer);
        serviceLocator.getAdminEndpoint().userChangeRoleAdmin(session, userId, role);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.ADMINISTRATOR }; }

}

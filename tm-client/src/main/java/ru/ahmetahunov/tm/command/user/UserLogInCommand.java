package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.AccessForbiddenException_Exception;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.io.IOException;

@NoArgsConstructor
public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "log-in";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception{
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[LOG IN]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @NotNull final String password = terminalService.getAnswer("Please enter password: ");
        @Nullable Session session = serviceLocator.getSessionEndpoint().createSession(login, password);
        serviceLocator.getStateService().setSession(session);
        terminalService.writeMessage("Welcome, " + login + "!");
    }

}

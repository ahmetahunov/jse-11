package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.command.AbstractCommand;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all users's available projects.";
    }

    @Override
    public void execute() throws AccessForbiddenException_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        serviceLocator.getProjectEndpoint().removeAllProjects(session);
        serviceLocator.getTerminalService().writeMessage("[ALL PROJECTS REMOVED]");
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}
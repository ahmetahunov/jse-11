package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

@NoArgsConstructor
public final class TaskDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "task-info";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task's information.";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception, FailedOperationException {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[TASK-DESCRIPTION]");
        @NotNull final String taskId = terminalService.getAnswer("Please enter task id: ");
        @Nullable final Task task = taskEndpoint.findOneTask(session, taskId);
        if (task == null) throw new FailedOperationException("Selected task does not exist.");
        terminalService.writeMessage(InfoUtil.getTaskInfo(task));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}

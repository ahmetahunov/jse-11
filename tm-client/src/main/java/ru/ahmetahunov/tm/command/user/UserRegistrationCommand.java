package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.api.endpoint.AccessForbiddenException_Exception;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import java.io.IOException;

@NoArgsConstructor
public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() {
        return "user-register";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "New user registration.";
    }

    @Override
    public void execute() throws IOException, FailedOperationException, AccessForbiddenException_Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[REGISTRATION]");
        @NotNull final String login = terminalService.getAnswer("Please enter login: ");
        @NotNull String password = terminalService.getAnswer("Please enter password: ");
        @NotNull final String repeatPass = terminalService.getAnswer("Please enter new password one more time: ");
        if (password.isEmpty() || !password.equals(repeatPass))
            throw new FailedOperationException("Passwords do not match!");
        serviceLocator.getUserEndpoint().createUser(login, password);
        terminalService.writeMessage("[OK]");
    }

}

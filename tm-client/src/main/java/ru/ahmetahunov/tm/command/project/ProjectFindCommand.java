package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.Project;
import ru.ahmetahunov.tm.api.endpoint.ProjectEndpoint;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.util.List;

@NoArgsConstructor
public class ProjectFindCommand extends AbstractCommand {

    @Override
    public boolean isSecure() { return false; }

    @NotNull
    @Override
    public String getName() { return "project-find"; }

    @NotNull
    @Override
    public String getDescription() { return "Search for project by part of name or description."; }

    @Override
    public void execute() throws Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[FIND PROJECT]");
        @NotNull final String searchPhrase =
                terminalService.getAnswer("Please enter project name or description: ");
        @NotNull final List<Project> projects = projectEndpoint.findProjectByNameOrDesc(session, searchPhrase);
        int i = 1;
        for (@NotNull final Project project : projects) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            project.getName(),
                            project.getId(),
                            project.getDescription()
                    )
            );
        }
        if (projects.isEmpty()) terminalService.writeMessage("Not found.");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}

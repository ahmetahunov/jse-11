package ru.ahmetahunov.tm.command;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.AccessForbiddenException_Exception;
import ru.ahmetahunov.tm.api.endpoint.InterruptOperationException_Exception;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.service.ServiceLocator;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Setter
    @NotNull
    protected ServiceLocator serviceLocator;

    public abstract boolean isSecure();

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception, AccessForbiddenException_Exception, InterruptOperationException_Exception;

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
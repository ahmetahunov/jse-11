package ru.ahmetahunov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectByDescriptionCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-by-description";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects with entered part of description.";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        terminalService.writeMessage("[PROJECTS BY DESCRIPTION]");
        @NotNull final String description = terminalService.getAnswer("Please enter project description: ");
        @NotNull final List<Project> projects = projectEndpoint.findProjectByDescription(session, description);
        int i = 1;
        for (@NotNull final Project project : projects) {
            terminalService.writeMessage(
                    String.format(
                            "%d.%s ID:%s\nDescription:%s",
                            i++,
                            project.getName(),
                            project.getId(),
                            project.getDescription()
                    )
            );
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.USER, Role.ADMINISTRATOR };
    }

}

package ru.ahmetahunov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.exception.FailedOperationException;
import ru.ahmetahunov.tm.util.StatusUtil;
import java.io.IOException;

@NoArgsConstructor
public final class TaskEditStatusCommand extends AbstractCommand {

    @Override
    public boolean isSecure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-edit-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit status of selected task.";
    }

    @Override
    public void execute() throws IOException, AccessForbiddenException_Exception, FailedOperationException {
        @Nullable final Session session = serviceLocator.getStateService().getSession();
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.writeMessage("[EDIT TASK STATUS]");
        @NotNull final String taskId = terminalService.getAnswer("Please enter task id: ");
        @Nullable final Task task = taskEndpoint.findOneTask(session, taskId);
        if (task == null) throw new FailedOperationException("Selected task does not exist.");
        @NotNull final String statusName = terminalService.getAnswer("Enter status<planned|in-progress|done>: ");
        @Nullable final Status status = StatusUtil.getStatus(statusName);
        task.setStatus(status);
        taskEndpoint.updateTask(session, task);
        terminalService.writeMessage("[OK]");
    }

    @Nullable
    @Override
    public Role[] getRoles() { return new Role[] { Role.USER, Role.ADMINISTRATOR }; }

}

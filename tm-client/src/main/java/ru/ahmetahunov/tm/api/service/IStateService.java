package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.util.Collection;

public interface IStateService {

    @NotNull
    public Collection<AbstractCommand> getCommands();

    @Nullable
    public Session getSession();

    public void setSession(Session session);

    public boolean isAllowedCommand(Role... roles);

    @Nullable
    public AbstractCommand getCommand(String operation);

}

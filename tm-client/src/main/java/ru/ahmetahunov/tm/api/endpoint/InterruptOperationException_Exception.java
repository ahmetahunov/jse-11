
package ru.ahmetahunov.tm.api.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.3.6
 * 2020-05-14T23:30:45.064+03:00
 * Generated source version: 3.3.6
 */

@WebFault(name = "InterruptOperationException", targetNamespace = "http://endpoint.api.tm.ahmetahunov.ru/")
public class InterruptOperationException_Exception extends Exception {

    private ru.ahmetahunov.tm.api.endpoint.InterruptOperationException interruptOperationException;

    public InterruptOperationException_Exception() {
        super();
    }

    public InterruptOperationException_Exception(String message) {
        super(message);
    }

    public InterruptOperationException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public InterruptOperationException_Exception(String message, ru.ahmetahunov.tm.api.endpoint.InterruptOperationException interruptOperationException) {
        super(message);
        this.interruptOperationException = interruptOperationException;
    }

    public InterruptOperationException_Exception(String message, ru.ahmetahunov.tm.api.endpoint.InterruptOperationException interruptOperationException, java.lang.Throwable cause) {
        super(message, cause);
        this.interruptOperationException = interruptOperationException;
    }

    public ru.ahmetahunov.tm.api.endpoint.InterruptOperationException getFaultInfo() {
        return this.interruptOperationException;
    }
}

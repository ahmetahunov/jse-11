package ru.ahmetahunov.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.Role;
import ru.ahmetahunov.tm.api.endpoint.Session;
import ru.ahmetahunov.tm.api.service.IStateService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import java.util.Collection;
import java.util.Map;

@RequiredArgsConstructor
public final class StateService implements IStateService {

    @NotNull
    private final Map<String, AbstractCommand> commands;

    @Getter
    @Setter
    @Nullable
    private Session session;

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    /*
    * Метод проверяет права на выполнение команды.
    * Метод вызывается, если команда не безопасна.
    * Если в метод передается null, значит команда доступна только вне учетной записи.
    * Если в метод передается пустой массив, значит команда не доступна никому, кроме системы.
    * В остальных случаях, согласно полученному массиву ролей.
     */
    @Override
    public boolean isAllowedCommand(@Nullable final Role ... roles) {
        if (roles == null && session == null) return true;
        if (session == null || roles == null) return false;
        for (Role role : roles) {
            if (session.getRole().equals(role)) return true;
        }
        return false;
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(@Nullable final String operation) {
        if (operation == null || operation.isEmpty()) return null;
        @Nullable final AbstractCommand command = commands.get(operation);
        if (command == null) return commands.get("unknown");
        if (command.isSecure()) return command;
        if (isAllowedCommand(command.getRoles())) return command;
        return commands.get("forbidden");
    }

}

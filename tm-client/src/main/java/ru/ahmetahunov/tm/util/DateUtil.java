package ru.ahmetahunov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {

    @NotNull
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    public static XMLGregorianCalendar parseDate(@NotNull final String date) {
        try {
            @NotNull final Date dateSimple = dateFormatter.parse(date);
            @NotNull final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(dateSimple.getTime());
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (ParseException | DatatypeConfigurationException e) {
            return null;
        }
    }

    @NotNull
    public static String formatDate(@Nullable final Date date) {
        if (date == null || date.equals(new Date(0))) return "not set";
        return dateFormatter.format(date);
    }

}
